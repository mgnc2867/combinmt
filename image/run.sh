#!/bin/sh

python3 OpenNMT-py/translate.py -model output/tables_and_plots/combiNMT995_epoch_15.pt -src /input/test.en -output output/datasets/combiNMT995-out.txt -replace_unk -verbose
python3 OpenNMT-py/translate.py -model output/tables_and_plots/combiNMT98_epoch_15.pt -src /input/test.en -output output/datasets/combiNMT98-out.txt -replace_unk -verbose
python3 OpenNMT-py/translate.py -model output/tables_and_plots/NTS-default-rep_epoch_15.pt -src /input/test.en -output output/datasets/NTS-default-rep-out.txt -replace_unk -verbose
python3 OpenNMT-py/translate.py -model output/tables_and_plots/NTSw2v-rep_epoch_15.pt -src /input/test.en -output output/datasets/NTSw2v-rep-out.txt -replace_unk -verbose
