from nltk.tokenize import sent_tokenize, word_tokenize
import warnings

warnings.filterwarnings(action = 'ignore')

import gensim
from gensim.models import Word2Vec

sample = open("../data/train.en")
s = sample.read()

f = s.replace("\n", " ")

data = []

for i in sent_tokenize(f):
    temp = []
    
    for j in word_tokenize(i):
        temp.append(j.lower())
        
        data.append(temp)
        
model = gensim.models.Word2Vec(data, min_count = 1, size = 200, window = 10, sg = 1, workers =4, iter = 1)
        
model.wv.save_word2vec_format('NTSw2v-enc-model')

sample = open("../data/train.sen")
s = sample.read()

f = s.replace("\n", " ")

data = []

for i in sent_tokenize(f):
    temp = []
    
    for j in word_tokenize(i):
        temp.append(j.lower())
        
        data.append(temp)
        
model = gensim.models.Word2Vec(data, min_count = 1, size = 200, window = 10, sg = 1, workers =4, iter = 1)
        
model.wv.save_word2vec_format('NTSw2v-dec-model')

sample = open("../data/original98.txt")
s = sample.read()

f = s.replace("\n", " ")

data = []

for i in sent_tokenize(f):
    temp = []
    
    for j in word_tokenize(i):
        temp.append(j.lower())
        
        data.append(temp)
        
model = gensim.models.Word2Vec(data, min_count = 1, size = 200, window = 10, sg = 1, workers =4, iter = 1)
        
model.wv.save_word2vec_format('combinmt98-enc-model')

sample = open("../data/simple98.txt")
s = sample.read()

f = s.replace("\n", " ")

data = []

for i in sent_tokenize(f):
    temp = []
    
    for j in word_tokenize(i):
        temp.append(j.lower())
        
        data.append(temp)
        
model = gensim.models.Word2Vec(data, min_count = 1, size = 200, window = 10, sg = 1, workers =4, iter = 1)
        
model.wv.save_word2vec_format('combinmt98-dec-model')

sample = open("../data/original995.txt")
s = sample.read()

f = s.replace("\n", " ")

data = []

for i in sent_tokenize(f):
    temp = []
    
    for j in word_tokenize(i):
        temp.append(j.lower())
        
        data.append(temp)
        
model = gensim.models.Word2Vec(data, min_count = 1, size = 200, window = 10, sg = 1, workers =4, iter = 1)
        
model.wv.save_word2vec_format('combinmt995-enc-model')

sample = open("../data/simple995.txt")
s = sample.read()

f = s.replace("\n", " ")

data = []

for i in sent_tokenize(f):
    temp = []
    
    for j in word_tokenize(i):
        temp.append(j.lower())
        
        data.append(temp)
        
model = gensim.models.Word2Vec(data, min_count = 1, size = 200, window = 10, sg = 1, workers =4, iter = 1)
        
model.wv.save_word2vec_format('combinmt98-dec-model')


