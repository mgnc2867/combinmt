import csv

cor1 =[]
cor2 =[]
r1 =[]
r2 =[]
r3 =[]
r4 =[]
total= 0
correct =0
results = open('/home/mgnc2867/OpenNMT-py/data/test-csv/results-combinmt995.csv', 'w')
writer = csv.writer(results)
with open('/home/mgnc2867/OpenNMT-py/data/test-csv/correct1/combinmt995-correct.csv') as csvfile:
	readcsv = csv.reader(csvfile, delimiter=',')
	for row in readcsv:
		cor1.append(row)
with open('/home/mgnc2867/OpenNMT-py/data/test-csv/correct2/test-combinmt995-correct.csv') as csvfile:
	readcsv = csv.reader(csvfile, delimiter=',')
	for row in readcsv:
		cor2.append(row)
with open('/home/mgnc2867/OpenNMT-py/data/test-csv/1/test-combinmt995-1.csv') as csvfile:
	readcsv = csv.reader(csvfile, delimiter=',')
	for row in readcsv:
		r1.append(row)
with open('/home/mgnc2867/OpenNMT-py/data/test-csv/2/test-combinmt995-2.csv') as csvfile:
	readcsv = csv.reader(csvfile, delimiter=',')
	for row in readcsv:
		r2.append(row)
with open('/home/mgnc2867/OpenNMT-py/data/test-csv/3/test-combinmt995-3.csv') as csvfile:
	readcsv = csv.reader(csvfile, delimiter=',')
	for row in readcsv:
		r3.append(row)
with open('/home/mgnc2867/OpenNMT-py/data/test-csv/4/test-combinmt995-4.csv') as csvfile:
	readcsv = csv.reader(csvfile, delimiter=',')
	for row in readcsv:
		r4.append(row)
count = 0
for row1 in cor1:
	if count == 0:
		count += 1
	else:
		if row1[3] == 'y':
			correct = correct +int(row1[2])
			total = total + int(row1[2])
		elif row1[3] == 'n':
			total = total + int(row1[2])
		else:
			count +=1
count = 0
for row1 in cor2:
	if count == 0:
		count += 1
	else:
		if row1[3] == 'y':
			correct = correct +int(row1[2])
			total = total + int(row1[2])
		elif row1[3] == 'n':
			total = total + int(row1[2])
			count +=1
		else:
			count +=1
count = 0
for row1, row2, row3, row4 in zip(r1, r2, r3, r4):
	if count ==0:
		line= [row1[0],row1[1],row1[2],row1[3]]
		writer.writerow(line)
		count +=1
	else:
		line = [row1[0], row1[1], ((int(row1[2])+int(row2[2])+int(row3[2])+int(row4[2]))/4), ((int(row1[3])+int(row2[3])+int(row3[3])+int(row4[3]))/4)]
		writer.writerow(line)
				
	
results.close()
total = (total/2)
correct = (correct/2)
print("Total number of changes: " + str(int(total)))
print("Total correct changes: " + str(int(correct)))
print("Percentage of correct changes: " + str(((correct/total)*100)) +"%")


cor1 =[]
cor2 =[]
r1 =[]
r2 =[]
r3 =[]
r4 =[]
total= 0
correct =0
results = open('/home/mgnc2867/OpenNMT-py/data/test-csv/results-combinmt98.csv', 'w')
writer = csv.writer(results)
with open('/home/mgnc2867/OpenNMT-py/data/test-csv/correct1/combinmt98-correct.csv') as csvfile:
	readcsv = csv.reader(csvfile, delimiter=',')
	for row in readcsv:
		cor1.append(row)
with open('/home/mgnc2867/OpenNMT-py/data/test-csv/correct2/test-combinmt98-correct.csv') as csvfile:
	readcsv = csv.reader(csvfile, delimiter=',')
	for row in readcsv:
		cor2.append(row)
with open('/home/mgnc2867/OpenNMT-py/data/test-csv/1/test-combinmt98-1.csv') as csvfile:
	readcsv = csv.reader(csvfile, delimiter=',')
	for row in readcsv:
		r1.append(row)
with open('/home/mgnc2867/OpenNMT-py/data/test-csv/2/test-combinmt98-2.csv') as csvfile:
	readcsv = csv.reader(csvfile, delimiter=',')
	for row in readcsv:
		r2.append(row)
with open('/home/mgnc2867/OpenNMT-py/data/test-csv/3/test-combinmt98-3.csv') as csvfile:
	readcsv = csv.reader(csvfile, delimiter=',')
	for row in readcsv:
		r3.append(row)
with open('/home/mgnc2867/OpenNMT-py/data/test-csv/4/test-combinmt98-4.csv') as csvfile:
	readcsv = csv.reader(csvfile, delimiter=',')
	for row in readcsv:
		r4.append(row)
count = 0
for row1 in cor1:
	if count == 0:
		count += 1
	else:
		if row1[3] == 'y':
			correct = correct +int(row1[2])
			total = total + int(row1[2])
		elif row1[3] == 'n':
			total = total + int(row1[2])
		else:
			count +=1
count = 0
for row1 in cor2:
	if count == 0:
		count += 1
	else:
		if row1[3] == 'y':
			correct = correct +int(row1[2])
			total = total + int(row1[2])
		elif row1[3] == 'n':
			total = total + int(row1[2])
			count +=1
		else:
			count +=1
count = 0
for row1, row2, row3, row4 in zip(r1, r2, r3, r4):
	if count ==0:
		line= [row1[0],row1[1],row1[2],row1[3]]
		writer.writerow(line)
		count +=1
	else:
		line = [row1[0], row1[1], ((int(row1[2])+int(row2[2])+int(row3[2])+int(row4[2]))/4), ((int(row1[3])+int(row2[3])+int(row3[3])+int(row4[3]))/4)]
		writer.writerow(line)
				
	
results.close()
total = (total/2)
correct = (correct/2)
print("Total number of changes: " + str(int(total)))
print("Total correct changes: " + str(int(correct)))
print("Percentage of correct changes: " + str(((correct/total)*100)) +"%")

	
cor1 =[]
cor2 =[]
r1 =[]
r2 =[]
r3 =[]
r4 =[]
total= 0
correct =0
results = open('/home/mgnc2867/OpenNMT-py/data/test-csv/results-NTS-default.csv', 'w')
writer = csv.writer(results)
with open('/home/mgnc2867/OpenNMT-py/data/test-csv/correct1/NTS-default-correct.csv') as csvfile:
	readcsv = csv.reader(csvfile, delimiter=',')
	for row in readcsv:
		cor1.append(row)
with open('/home/mgnc2867/OpenNMT-py/data/test-csv/correct2/test-NTS-default-correct.csv') as csvfile:
	readcsv = csv.reader(csvfile, delimiter=',')
	for row in readcsv:
		cor2.append(row)
with open('/home/mgnc2867/OpenNMT-py/data/test-csv/1/test-NTS-default-1.csv') as csvfile:
	readcsv = csv.reader(csvfile, delimiter=',')
	for row in readcsv:
		r1.append(row)
with open('/home/mgnc2867/OpenNMT-py/data/test-csv/2/test-NTS-default-2.csv') as csvfile:
	readcsv = csv.reader(csvfile, delimiter=',')
	for row in readcsv:
		r2.append(row)
with open('/home/mgnc2867/OpenNMT-py/data/test-csv/3/test-NTS-default-3.csv') as csvfile:
	readcsv = csv.reader(csvfile, delimiter=',')
	for row in readcsv:
		r3.append(row)
with open('/home/mgnc2867/OpenNMT-py/data/test-csv/4/test-NTS-default-4.csv') as csvfile:
	readcsv = csv.reader(csvfile, delimiter=',')
	for row in readcsv:
		r4.append(row)
count = 0
for row1 in cor1:
	if count == 0:
		count += 1
	else:
		if row1[3] == 'y':
			correct = correct +int(row1[2])
			total = total + int(row1[2])
		elif row1[3] == 'n':
			total = total + int(row1[2])
		else:
			count +=1
count = 0
for row1 in cor2:
	if count == 0:
		count += 1
	else:
		if row1[3] == 'y':
			correct = correct +int(row1[2])
			total = total + int(row1[2])
		elif row1[3] == 'n':
			total = total + int(row1[2])
			count +=1
		else:
			count +=1
count = 0
for row1, row2, row3, row4 in zip(r1, r2, r3, r4):
	if count ==0:
		line= [row1[0],row1[1],row1[2],row1[3]]
		writer.writerow(line)
		count +=1
	else:
		line = [row1[0], row1[1], ((int(row1[2])+int(row2[2])+int(row3[2])+int(row4[2]))/4), ((int(row1[3])+int(row2[3])+int(row3[3])+int(row4[3]))/4)]
		writer.writerow(line)
				
	
results.close()
total = (total/2)
correct = (correct/2)
print("Total number of changes: " + str(int(total)))
print("Total correct changes: " + str(int(correct)))
print("Percentage of correct changes: " + str(((correct/total)*100)) +"%")


cor1 =[]
cor2 =[]
r1 =[]
r2 =[]
r3 =[]
r4 =[]
total= 0
correct =0
results = open('/home/mgnc2867/OpenNMT-py/data/test-csv/results-NTS-default-rep.csv', 'w')
writer = csv.writer(results)
with open('/home/mgnc2867/OpenNMT-py/data/test-csv/correct1/NTS-default-rep-correct.csv') as csvfile:
	readcsv = csv.reader(csvfile, delimiter=',')
	for row in readcsv:
		cor1.append(row)
with open('/home/mgnc2867/OpenNMT-py/data/test-csv/correct2/test-NTS-default-rep-correct.csv') as csvfile:
	readcsv = csv.reader(csvfile, delimiter=',')
	for row in readcsv:
		cor2.append(row)
with open('/home/mgnc2867/OpenNMT-py/data/test-csv/1/test-NTS-default-rep-1.csv') as csvfile:
	readcsv = csv.reader(csvfile, delimiter=',')
	for row in readcsv:
		r1.append(row)
with open('/home/mgnc2867/OpenNMT-py/data/test-csv/2/test-NTS-default-rep-2.csv') as csvfile:
	readcsv = csv.reader(csvfile, delimiter=',')
	for row in readcsv:
		r2.append(row)
with open('/home/mgnc2867/OpenNMT-py/data/test-csv/3/test-NTS-default-rep-3.csv') as csvfile:
	readcsv = csv.reader(csvfile, delimiter=',')
	for row in readcsv:
		r3.append(row)
with open('/home/mgnc2867/OpenNMT-py/data/test-csv/4/test-NTS-default-rep-4.csv') as csvfile:
	readcsv = csv.reader(csvfile, delimiter=',')
	for row in readcsv:
		r4.append(row)
count = 0
for row1 in cor1:
	if count == 0:
		count += 1
	else:
		if row1[3] == 'y':
			correct = correct +int(row1[2])
			total = total + int(row1[2])
		elif row1[3] == 'n':
			total = total + int(row1[2])
		else:
			count +=1
count = 0
for row1 in cor2:
	if count == 0:
		count += 1
	else:
		if row1[3] == 'y':
			correct = correct +int(row1[2])
			total = total + int(row1[2])
		elif row1[3] == 'n':
			total = total + int(row1[2])
			count +=1
		else:
			count +=1
count = 0
for row1, row2, row3, row4 in zip(r1, r2, r3, r4):
	if count ==0:
		line= [row1[0],row1[1],row1[2],row1[3]]
		writer.writerow(line)
		count +=1
	else:
		line = [row1[0], row1[1], ((int(row1[2])+int(row2[2])+int(row3[2])+int(row4[2]))/4), ((int(row1[3])+int(row2[3])+int(row3[3])+int(row4[3]))/4)]
		writer.writerow(line)
				
	
results.close()
total = (total/2)
correct = (correct/2)
print("Total number of changes: " + str(int(total)))
print("Total correct changes: " + str(int(correct)))
print("Percentage of correct changes: " + str(((correct/total)*100)) +"%")


cor1 =[]
cor2 =[]
r1 =[]
r2 =[]
r3 =[]
r4 =[]
total= 0
correct =0
results = open('/home/mgnc2867/OpenNMT-py/data/test-csv/results-NTSw2v.csv', 'w')
writer = csv.writer(results)
with open('/home/mgnc2867/OpenNMT-py/data/test-csv/correct1/NTSw2v-correct.csv') as csvfile:
	readcsv = csv.reader(csvfile, delimiter=',')
	for row in readcsv:
		cor1.append(row)
with open('/home/mgnc2867/OpenNMT-py/data/test-csv/correct2/test-NTSw2v-correct.csv') as csvfile:
	readcsv = csv.reader(csvfile, delimiter=',')
	for row in readcsv:
		cor2.append(row)
with open('/home/mgnc2867/OpenNMT-py/data/test-csv/1/test-NTSw2v-1.csv') as csvfile:
	readcsv = csv.reader(csvfile, delimiter=',')
	for row in readcsv:
		r1.append(row)
with open('/home/mgnc2867/OpenNMT-py/data/test-csv/2/test-NTSw2v-2.csv') as csvfile:
	readcsv = csv.reader(csvfile, delimiter=',')
	for row in readcsv:
		r2.append(row)
with open('/home/mgnc2867/OpenNMT-py/data/test-csv/3/test-NTSw2v-3.csv') as csvfile:
	readcsv = csv.reader(csvfile, delimiter=',')
	for row in readcsv:
		r3.append(row)
with open('/home/mgnc2867/OpenNMT-py/data/test-csv/4/test-NTSw2v-4.csv') as csvfile:
	readcsv = csv.reader(csvfile, delimiter=',')
	for row in readcsv:
		r4.append(row)
count = 0
for row1 in cor1:
	if count == 0:
		count += 1
	else:
		if row1[3] == 'y':
			correct = correct +int(row1[2])
			total = total + int(row1[2])
		elif row1[3] == 'n':
			total = total + int(row1[2])
		else:
			count +=1
count = 0
for row1 in cor2:
	if count == 0:
		count += 1
	else:
		if row1[3] == 'y':
			correct = correct +int(row1[2])
			total = total + int(row1[2])
		elif row1[3] == 'n':
			total = total + int(row1[2])
		else:
			count +=1
count = 0
for row1, row2, row3, row4 in zip(r1, r2, r3, r4):
	if count ==0:
		line= [row1[0],row1[1],row1[2],row1[3]]
		writer.writerow(line)
		count +=1
	else:
		line = [row1[0], row1[1], ((int(row1[2])+int(row2[2])+int(row3[2])+int(row4[2]))/4), ((int(row1[3])+int(row2[3])+int(row3[3])+int(row4[3]))/4)]
		writer.writerow(line)
				
	
results.close()
total = (total/2)
correct = (correct/2)
print("Total number of changes: " + str(int(total)))
print("Total correct changes: " + str(int(correct)))
print("Percentage of correct changes: " + str(((correct/total)*100)) +"%")

results = open('/home/mgnc2867/OpenNMT-py/data/test-csv/results-combinmt995.csv','a')
writer = csv.writer(results)
file =[]
number = 0.0
numbers = 0.0
with open('/home/mgnc2867/OpenNMT-py/data/test-csv/results-combinmt995.csv') as csvfile:
	readcsv = csv.reader(csvfile,delimiter=',')
	for row in readcsv:
		file.append(row)
count = 0
count1 = 0
for row in file:
	if count == 0:
		count+=1
	elif float(row[2]) != 0:
		number = number + float(row[2])
		numbers = numbers + float(row[3])
		count += 1
	else:
		count1 += 1


gram = number / count -1
mean = numbers / count  -1

append = ['','',str(gram),str(mean)]
writer.writerow(append)

print('combiNMT995' + '\n'+ 'grammaticality mean: ' + str(gram/2) +'\n' + 'meaning preservation mean: ' + str(mean/2))

results.close()
results = open('/home/mgnc2867/OpenNMT-py/data/test-csv/results-combinmt98.csv','a')
writer = csv.writer(results)
file =[]
number = 0.0
numbers = 0.0
with open('/home/mgnc2867/OpenNMT-py/data/test-csv/results-combinmt98.csv') as csvfile:
	readcsv = csv.reader(csvfile,delimiter=',')
	for row in readcsv:
		file.append(row)
count = 0
count1 = 0
for row in file:
	if count == 0:
		count+=1
	elif float(row[2]) != 0:
		number = number + float(row[2])
		numbers = numbers + float(row[3])
		count += 1
	else:
		count1 += 1


gram = number / count -1
mean = numbers / count  -1


append = ['','',str(gram),str(mean)]
writer.writerow(append)


print('combiNMT98' + '\n'+ 'grammaticality mean: ' + str(gram/2) +'\n' + 'meaning preservation mean: ' + str(mean/2))
results.close()

results = open('/home/mgnc2867/OpenNMT-py/data/test-csv/results-NTS-default.csv','a')
writer = csv.writer(results)
file =[]
number = 0.0
numbers = 0.0
with open('/home/mgnc2867/OpenNMT-py/data/test-csv/results-NTS-default.csv') as csvfile:
	readcsv = csv.reader(csvfile,delimiter=',')
	for row in readcsv:
		file.append(row)
count = 0
count1 = 0
for row in file:
	if count == 0:
		count+=1
	elif float(row[2]) != 0:
		number = number + float(row[2])
		numbers = numbers + float(row[3])
		count += 1
	else:
		count1 += 1


gram = number / count -1
mean = numbers / count  -1


append = ['','',str(gram),str(mean)]
writer.writerow(append)


print('NTS-default' + '\n'+ 'grammaticality mean: ' + str(gram/2) +'\n' + 'meaning preservation mean: ' + str(mean/2))
results.close()
results = open('/home/mgnc2867/OpenNMT-py/data/test-csv/results-NTS-default-rep.csv','a')
writer = csv.writer(results)
file =[]
number = 0.0
numbers = 0.0
with open('/home/mgnc2867/OpenNMT-py/data/test-csv/results-NTS-default-rep.csv') as csvfile:
	readcsv = csv.reader(csvfile,delimiter=',')
	for row in readcsv:
		file.append(row)
count = 0
count1 = 0
for row in file:
	if count == 0:
		count+=1
	elif float(row[2]) != 0:
		number = number + float(row[2])
		numbers = numbers + float(row[3])
		count += 1
	else:
		count1 += 1


gram = number / count -1
mean = numbers / count  -1


append = ['','',str(gram),str(mean)]
writer.writerow(append)


print('NTS-default-rep' + '\n'+ 'grammaticality mean: ' + str(gram/2) +'\n' + 'meaning preservation mean: ' + str(mean/2))
results.close()

results = open('/home/mgnc2867/OpenNMT-py/data/test-csv/results-NTSw2v.csv','a')
writer = csv.writer(results)
file =[]
number = 0.0
numbers = 0.0
with open('/home/mgnc2867/OpenNMT-py/data/test-csv/results-NTSw2v.csv') as csvfile:
	readcsv = csv.reader(csvfile,delimiter=',')
	for row in readcsv:
		file.append(row)
count = 0
count1 = 0
for row in file:
	if count == 0:
		count+=1
	elif float(row[2]) != 0:
		number = number + float(row[2])
		numbers = numbers + float(row[3])
		count += 1
	else:
		count1 += 1


gram = number / count -1
mean = numbers / count  -1

append = ['','',str(gram),str(mean)]
writer.writerow(append)


print('NTSw2v' + '\n'+ 'grammaticality mean: ' + str(gram/2) +'\n' + 'meaning preservation mean: ' + str(mean/2))


